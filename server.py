# encoding: utf-8

from flask import Flask, session
from flask_babel import Babel
from flask_login import LoginManager

from app import admin, api, db, migrate
from app.routes import routes, apis
from app.model.user import get_user
from command import commands


application = Flask(__name__, template_folder="app/view")
application.config.from_object("config")
application.jinja_env.trim_blocks = application.config.get("JINJA_ENV", {}).get(
    "TRIM_BLOCKS", False
)
application.jinja_env.lstrip_blocks = application.config.get("JINJA_ENV", {}).get(
    "LSTRIP_BLOCKS", False
)

for route in routes:
    # Default routing on GET method only
    if len(route) < 3:
        application.add_url_rule(route[0], route[1].__name__, route[1], methods=["GET"])
    else:
        application.add_url_rule(
            route[0], route[1].__name__, route[1], methods=route[2]
        )
for route in apis:
    api.add_resource(route[1], route[0])

admin.init_app(application)
api.init_app(application)
babel = Babel(application)
login_manager = LoginManager(application)
db.init_app(application)
migrate.init_app(application, db)


@babel.localeselector
def get_locale():
    if "locale" not in session:
        session["locale"] = application.config.get(
            "BABEL_DEFAULT_LOCALE",
        )
    return session["locale"]


@login_manager.user_loader
def load_user(user_id):
    return get_user(user_id)


for command in commands:
    application.cli.add_command(command)
