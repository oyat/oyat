# encoding: utf-8

import os

DEBUG = False
SECRET_KEY = "Choose a secret key"

JINJA_ENV = {
    "TRIM_BLOCKS": True,
    "LSTRIP_BLOCKS": True,
}

# defining base directory
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

# defining database URI
# SQLALCHEMY_DATABASE_URI = "mysql://username:password@server/db"
SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(BASE_DIR, "db.sqlite3")
SQLALCHEMY_TRACK_MODIFICATIONS = False

# defining Bcrypt rounds to use
BCRYPT_ROUNDS = 15

# defining Babel settings
BABEL_DEFAULT_LOCALE = "fr"

# Languages available
AVAILABLE_LANGUAGES = {
    "en": "English",
    "fr": "French",
}
