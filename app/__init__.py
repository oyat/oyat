# encoding: utf-8

from flask_admin import Admin, expose, AdminIndexView
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

from app.form.user import UserLoginForm


class IndexView(AdminIndexView):
    @expose("/")
    def index(self):
        return self.render("admin/index.html", form=UserLoginForm())


admin = Admin(index_view=IndexView())
api = Api()
db = SQLAlchemy()
migrate = Migrate()
