# encoding: utf-8

from app.controller.admin import Admin
from app.controller.core import Core

routes = [
    ("/", Core.as_view("home")),
    ("/admin/login", Admin.as_view("login"), ["GET", "POST"]),
]

apis = []
