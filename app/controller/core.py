# encoding: utf-8

from flask import render_template

from app.controller.controller import Controller


class Core(Controller):
    def home(self):
        return render_template("core/home.html")
